//
//  FeedInteractorSpec.swift
//  SocialFeedTests
//
//  Created by Angel Landoni on 11/26/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

import Quick
import Nimble

@testable import SocialFeed

class FeedInteractorSepc: QuickSpec {
    override func spec() {
        
        class FeedPresenterMock: FeedInteractorPresenterTypes {
            var view: ViewLinks?
            var wireframe: WireframeLinks?
            var interactor: InteractorLinks?
            
            var retreivePosts: [PostModel]? = nil
            
            func onViewDidLoad() {
                
            }
            
            func onViewWillAppear() {
                
            }
            
            func retreivePosts(posts: [PostModel]) {
                retreivePosts = posts
            }
        }
        
        class SocialAPIMock: SocialAPI {
            override func loadFeeds(withLimit limit: Int, andCallback callback: @escaping ([SocialAPIPostModel]) -> Void) {
                var mockArray: [SocialAPIPostModel] = []
                mockArray.append(SocialAPIPostModel(text: "Bat", image: "Man"))
                callback(mockArray)
            }
            
            override func searchFeeds(byText text: String, andCallback callback: @escaping ([SocialAPIPostModel]) -> Void) {
                var mockArray: [SocialAPIPostModel] = []
                mockArray.append(SocialAPIPostModel(text: "Super", image: "Fish"))
                callback(mockArray)
            }
        }
        
        class LocalPostDataManagerMock: LocalPostDataManager {
            var textSaved: String = ""
            
            override func saveNewSearch(withText text: String) {
                textSaved = text
            }
        }
        
        var subject: FeedInteractor<FeedPresenterMock>? = nil
        var presenter: FeedPresenterMock? = nil
        let socialAPI: SocialAPI = SocialAPIMock()
        let localPostDataManager: LocalPostDataManagerMock = LocalPostDataManagerMock()
        
        beforeEach {
            subject = FeedInteractor<FeedPresenterMock>()
            presenter = FeedPresenterMock()
            
            subject?.presenter = presenter
            subject?.socialAPI = socialAPI
            subject?.localPostDataManager = localPostDataManager
        }
        
        describe("loadBasicFeeds") {
            beforeEach {
                subject?.loadBasicFeeds()
            }
            
            it("should return the correct object, not be nil") {
                expect(presenter?.retreivePosts).toNot(beNil())
                expect(presenter?.retreivePosts?.first?.text) == "Bat"
                expect(presenter?.retreivePosts?.first?.image) == "Man"
            }
        }
        
        describe("loadSpecificFeed") {
            beforeEach {
                subject?.loadSpecificFeed(withText: "RubixCube")
            }
            
            it("should call the correct save search method") {
                expect(localPostDataManager.textSaved) == "RubixCube"
            }
            
            it("should return the correct object, not be nil") {
                expect(presenter?.retreivePosts).toNot(beNil())
                expect(presenter?.retreivePosts?.first?.text) == "Super"
                expect(presenter?.retreivePosts?.first?.image) == "Fish"
            }
        }
    }
}
