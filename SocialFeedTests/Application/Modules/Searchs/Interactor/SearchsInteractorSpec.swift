//
//  SearchsViewTest.swift
//  SocialFeedTests
//
//  Created by Angel Landoni on 11/26/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

import Quick
import Nimble

@testable import SocialFeed

class SearchsInteractorSepc: QuickSpec {
    override func spec() {
        
        class SearchsLocalPostDataManagerMock: SearchsLocalPostDataManager {
            override func loadAllSearchs() -> [LocalSearchModel] {
                var searchs: [LocalSearchModel] = []
                var model = LocalSearchModel()
                model.search = "MOCKDATA"
                searchs.append(model)
                return searchs
            }
        }
        
        class SearchsPresenterMock: SearchsInteractorPresenterTypes {
            var view: ViewLinks?
            var wireframe: WireframeLinks?
            var interactor: InteractorLinks?
            
            var mockedData: [SearchModel]? = nil
            
            func retrieveSearchs(searchs: [SearchModel]) {
                mockedData = searchs
            }
            
            func onViewDidLoad() {}
            func onViewWillAppear() {}
        }

        
        var subject: SearchsInteractor<SearchsPresenterMock>? = nil
        var dataManager: SearchsLocalPostDataManagerMock? = nil
        var presenter: SearchsPresenterMock? = nil
        
        beforeEach {
            subject = SearchsInteractor<SearchsPresenterMock>()
            dataManager = SearchsLocalPostDataManagerMock()
            presenter = SearchsPresenterMock()
            
            subject?.presenter = presenter
            subject?.dataManager = dataManager!
        }
        
        describe("loadAllSearchs") {
            beforeEach {
                subject?.loadAllSearchs()
            }
            
            it("should not be nil") {
                expect(presenter?.mockedData).toNot(beNil())
            }
        }
    }
}
