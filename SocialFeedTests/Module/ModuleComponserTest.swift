//
//  ModuleComponserTest.swift
//  SocialFeedTests
//
//  Created by Angel Landoni on 11/26/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

import Quick
import Nimble

@testable import SocialFeed

class ModuleComposerSpec: QuickSpec {
    override func spec() {
        
        class ViewMock: Initializer, ViewLinks {
            var presenter: PresenterLinks?
            required init() {}
        }
        
        class PresenterMock: Initializer, PresenterLinks {
            var view: ViewLinks?
            var wireframe: WireframeLinks?
            var interactor: InteractorLinks?
            required init() {}
        }
        
        class InteractorMock: Initializer, InteractorLinks {
            var presenter: PresenterLinks?
            required init() {}
        }
        
        class WireframeMock: Initializer, WireframeLinks {
            var presenter: PresenterLinks?
            required init() {}
        }
        
        var subject: ModuleComposer<ViewMock,PresenterMock,InteractorMock,WireframeMock>? = nil
        
        beforeEach {
            subject = ModuleComposer<ViewMock,PresenterMock,InteractorMock,WireframeMock>()
        }
        
        describe("init") {
            it("should link all the components") {
                expect(subject?.view.presenter).toNot(beNil())
                expect(subject?.presenter.view).toNot(beNil())
                expect(subject?.presenter.interactor).toNot(beNil())
                expect(subject?.presenter.wireframe).toNot(beNil())
                expect(subject?.interactor.presenter).toNot(beNil())
            }
        }
    }
}
