//
//  PresenterLinks.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/24/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

protocol PresenterLinks: class {
    var view: ViewLinks? { get set }
    var wireframe: WireframeLinks? { get set }
    var interactor: InteractorLinks? { get set }
}
