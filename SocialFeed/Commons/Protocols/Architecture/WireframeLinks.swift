//
//  WireframeLinks.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/24/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

protocol WireframeLinks: class {
    var presenter: PresenterLinks? { get set }
}
