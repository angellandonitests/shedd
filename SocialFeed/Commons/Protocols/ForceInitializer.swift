//
//  Initializer.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/24/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

/// This protocol forces the class to have a constructor.
protocol Initializer: class {
    init()
}
