//
//  SFViewController.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/24/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

import UIKit

class SFViewController: UIViewController {
    /// Basic constructor.
    init() {
        super.init(nibName: nil, bundle: nil)
        setup()
        onInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
        onInit()
    }
    
    // MARK: - Private methods
    
    /**
     This method setups the entire component.
     */
    private func setup() {
        // Setups the background color.
        view.backgroundColor = UIColor.white
    }
    
    // MARK: - Override methods
    
    func onInit() { /* Override me! */ }
}
