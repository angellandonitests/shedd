//
//  SFWindow.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/24/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

import UIKit

class SFWindow: UIWindow {
    /**
     Basic constructor.
     */
    init() {
        super.init(frame: UIScreen.main.bounds)
        setup()
    }
    /**
     Required constructor.
     */
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - Private methods
    
    private func setup() { makeKeyAndVisible() }
}

