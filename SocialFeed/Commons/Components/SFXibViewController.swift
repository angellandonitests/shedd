//
//  SFXibViewController.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/24/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

import UIKit

class SFXibViewController: UIViewController {
    /// Init the View controller with a xib, remember
    /// the xib must have the same name of the class!.
    init() {
        super.init(
            nibName: String(describing: type(of: self)).components(separatedBy: ["<"])[0],
            bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) { super.init(coder: aDecoder) }
}
