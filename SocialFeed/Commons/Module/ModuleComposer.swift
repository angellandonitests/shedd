//
//  ModuleComposer.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/24/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

import UIKit

struct ModuleComposer<TView: Initializer & ViewLinks,
                        RPresenter: Initializer & PresenterLinks,
                        PInteractor: Initializer & InteractorLinks,
                        LWireframe: Initializer & WireframeLinks> {
    private var viewRef: TView! = nil
    private var presenterRef: RPresenter! = nil
    private var wireframeRef: LWireframe! = nil
    private var interactorRef: PInteractor! = nil
    
    init() { setup() }
    
    // MARK: - Private methods
    
    private mutating func setup() {
        viewRef = TView()
        presenterRef = RPresenter()
        interactorRef = PInteractor()
        wireframeRef = LWireframe()
        link()
    }
    
    // MARK: - Public methods
    
    /// This method links all the components.
    func link() {
        // View -> Presenter
        viewRef.presenter = presenterRef
        // Presenter -> View
        presenterRef.view = viewRef
        // Presenter -> Interactor
        presenterRef.interactor = interactorRef
        // Presenter -> Wireframe
        presenterRef.wireframe = wireframeRef
        // Wireframe -> Presenter
        wireframeRef.presenter = presenterRef
        // Interactor -> Presenter
        interactorRef.presenter = presenterRef
    }
    
    // MARK: - Getters and Setters
    
    var view: TView { return viewRef }
    var presenter: RPresenter { return presenterRef }
    var wireframe: LWireframe { return wireframeRef }
    var interactor: PInteractor { return interactorRef }
}

