//
//  Class.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/25/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

/// This method retuns the name of a object's class.
func getClassName<T>(object: T) -> String {
    // Returns the name of the class and avoid the generic part :).
    return String(describing: type(of: object)).components(separatedBy: ["<"])[0]
}
