//
//  Localized.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/26/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

import Foundation

enum Localized {
    /// Loads a buffer from a localized.
    /// - parameters: withKey: Key to load.
    ///               andTable: Name of the file.
    static func load(withKey key: String, andTable table: String) -> String {
        return NSLocalizedString(key,
                                 tableName: table,
                                 bundle: Bundle.main,
                                 value: "",
                                 comment: "")
    }
}
