//
//  LunchConfig.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/24/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

import UIKit

/// A bouch of properties used to setup the startup of the
/// application.
enum LaunchConfig {
    /// Module used to start the application.
    static let mainModule = LoginModuleComposer.self
    /// Number of items to load in the init of the app.
    static let numberOfFeedsToLoad: Int = 50
}
