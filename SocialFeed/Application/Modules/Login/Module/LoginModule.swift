//
//  LoginModule.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/24/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

// Wrappes the annoying long type.
typealias LoginModuleComposer = ModuleComposer<
    LoginView<LoginPresenter>,
    LoginPresenter,
    LoginInteractor<LoginPresenter>,
    LoginWireframe>

/**
 This method retuns the basic games list entire section.
 */
func getLoginModule() -> LoginModuleComposer {
    let module: LoginModuleComposer = LoginModuleComposer()
    return module
}
