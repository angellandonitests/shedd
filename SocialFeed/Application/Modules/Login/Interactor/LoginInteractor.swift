//
//  LoginInteractor.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/24/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

/// Requerimients needed to setup the presenter into the view.
typealias LoginInteractorPresenterTypes =
    LoginPresenterSignIn &
    PresenterLinks

final class LoginInteractor<TPresenter: LoginInteractorPresenterTypes> {
    private weak var presenterRef: TPresenter? = nil
}

// MARK: - Login Presenter Events

extension LoginInteractor: LoginPresenterEvents {
    /// Login into the social media
    func login() {
        SocialAPI().login { (state: Bool) in
            if state {
                // Calling the presenter good boy.
                self.presenterRef?.onLoginSuccess()
            } else {
                // Calling the presetner bad boy.
                self.presenterRef?.onLoginFailure()
            }
        }
    }
}

// MARK: - Force initializer

extension LoginInteractor: Initializer { }

// MARK: - Interactor Links

extension LoginInteractor: InteractorLinks {
 
    var presenter: PresenterLinks? {
        get { return presenterRef }
        set { presenterRef = newValue as? TPresenter }
    }
}
