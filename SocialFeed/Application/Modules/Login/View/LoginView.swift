//
//  LoginView.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/24/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

import UIKit

import TwitterKit

/// Requerimients needed to setup the presenter into the view.
typealias LoginViewPresenterTypes =
    ViewControllerEvents &
    LoginViewActions &
    PresenterLinks

final class LoginView<T: LoginViewPresenterTypes>: SFXibViewController {
    // MARK: - Properties
    private var presenterRef: T? = nil

    // MARK: - View Controller methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenterRef?.onViewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenterRef?.onViewWillAppear()
    }
    
    override func didReceiveMemoryWarning() { super.didReceiveMemoryWarning() }
    
    // MARK: - UI Events / Actions
    
    @IBAction func onTapTwitterLoginButton(_ sender: UIButton) {
        presenterRef?.onTapLoginButton(socialMedia: .Twitter)
    }
    
    @IBAction func onTapFacebookLoginButton(_ sender: UIButton) {
        presenterRef?.onTapLoginButton(socialMedia: .Facebook)
    }
}

// MARK: - Force Initializer

extension LoginView: Initializer {  }


// MARK: - View Links

extension LoginView: ViewLinks {
    
    var presenter: PresenterLinks? {
        get { return presenterRef }
        set { presenterRef = newValue as? T }
    }  
}

