//
//  LoginWireframe.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/24/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

import UIKit

final class LoginWireframe {
    private weak var presenterRef: LoginPresenter? = nil
    
    func moveToTheApplication() {
        DispatchQueue.main.async {
            // Feed Module
            let feedModule: FeedModuleComposer = FeedModuleComposer()
            let searchsModule: SearchsModuleComposer = SearchsModuleComposer()
            
            let mainTabBarController: SFTabBarController = SFTabBarController()
            mainTabBarController.viewControllers = [
                feedModule.view,
                searchsModule.view
            ]
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = mainTabBarController
        }
    }
}

// MARK: - Force initializer

extension LoginWireframe: Initializer { }

// MARK: - Wireframe Links

extension LoginWireframe: WireframeLinks {
    
    var presenter: PresenterLinks? {
        get { return presenterRef }
        set { presenterRef = newValue as? LoginPresenter }
    }
}
