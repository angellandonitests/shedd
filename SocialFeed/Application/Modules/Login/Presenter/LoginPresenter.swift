//
//  LoginPresenter.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/24/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

final class LoginPresenter {
    private weak var viewRef: LoginView<LoginPresenter>? = nil
    private var interactorRef: LoginInteractor<LoginPresenter>? = nil
    private var wireframeRef: LoginWireframe? = nil
}

// MARK: - View Controller events

extension LoginPresenter: ViewControllerEvents {

    func onViewDidLoad() {
        
    }
    
    func onViewWillAppear() {
        
    }
}

// MARK: - Login view actions

extension LoginPresenter: LoginViewActions {
    /// This method executes the correct interactor method depending
    /// the social media needed.
    func onTapLoginButton(socialMedia: SocialAPIState) {
        // Setups the social media selected.
        SocialAPI().setup(socialMedia: socialMedia)
        interactorRef?.login()
    }
}

// MARK: - Login Presenter SingIn

extension LoginPresenter: LoginPresenterSignIn {
    /// This method should be executed when the login is success.
    /// It calls the wireframe in order to move to the main section.
    func onLoginSuccess() {
        wireframeRef?.moveToTheApplication()
    }
    
    func onLoginFailure() {
        // TODO: Show Some Crasy View or alert
    }
}

// MARK: - Force initializer

extension LoginPresenter: Initializer {  }

// MARk: - Presenter links

extension LoginPresenter: PresenterLinks {
    
    var view: ViewLinks? {
        get { return viewRef }
        set { viewRef = newValue as? LoginView }
    }
    
    var interactor: InteractorLinks? {
        get { return interactorRef }
        set { interactorRef = newValue as? LoginInteractor }
    }

    var wireframe: WireframeLinks? {
        get { return wireframeRef }
        set { wireframeRef = newValue as? LoginWireframe }
    }
}
