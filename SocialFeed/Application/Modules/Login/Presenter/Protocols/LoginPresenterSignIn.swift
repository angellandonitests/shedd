//
//  LoginPresenterSignIn.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/25/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

protocol LoginPresenterSignIn: class {
    func onLoginSuccess()
    func onLoginFailure()
}
