//
//  LoginPresenterEvents.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/25/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

protocol LoginPresenterEvents: class {
    func login()
}
