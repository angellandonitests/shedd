//
//  SearchsPresenter.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/26/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

final class SearchsPresenter {
    private weak var viewRef: SearchsView<SearchsPresenter>? = nil
    private var interactorRef: SearchsInteractor<SearchsPresenter>? = nil
    private var wireframeRef: SearchsWireframe<SearchsPresenter>? = nil
}

// MARK: - View controller events

extension SearchsPresenter: ViewControllerEvents {
    /// This method will be executed when the view enters in the
    /// view did load.
    func onViewDidLoad() {
        
    }
    
    /// This method will be fired when the view enters in the
    /// view will appear.
    func onViewWillAppear() {
        interactorRef?.loadAllSearchs()
    }
}

// MARK: - Searchs Presenter Retreiver

extension SearchsPresenter: SearchsPresenterRetreive {
    /// This method updates all the searchs into the view.
    /// - parameters: searchs: A list of searchs.
    func retrieveSearchs(searchs: [SearchModel]) {
        // Check if there are not searchs.
        guard !searchs.isEmpty else { return }
        // Update the view with the correct searchs.
        viewRef?.updateSearchList(posts: searchs.map({ (search: SearchModel) -> String in
            return search.text
        }))
    }
}

// MARK: - Force initializer

extension SearchsPresenter: Initializer {  }

// MARk: - Presenter links

extension SearchsPresenter: PresenterLinks {
    
    var view: ViewLinks? {
        get { return viewRef }
        set { viewRef = newValue as? SearchsView }
    }
    
    var interactor: InteractorLinks? {
        get { return interactorRef }
        set { interactorRef = newValue as? SearchsInteractor }
    }
    
    var wireframe: WireframeLinks? {
        get { return wireframeRef }
        set { wireframeRef = newValue as? SearchsWireframe }
    }
}
