//
//  SearchsWireframe.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/26/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

typealias SearchsWireframePresenterTypes =
    PresenterLinks

final class SearchsWireframe<TPresenter: SearchsViewPresenterTypes> {
    private weak var presenterRef: TPresenter? = nil
}

// MARK: - Force initializer

extension SearchsWireframe: Initializer { }

// MARK: - Wireframe Links

extension SearchsWireframe: WireframeLinks {
    
    var presenter: PresenterLinks? {
        get { return presenterRef }
        set { presenterRef = newValue as? TPresenter }
    }
}
