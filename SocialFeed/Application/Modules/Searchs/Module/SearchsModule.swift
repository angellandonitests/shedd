//
//  SearchsModule.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/26/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

// Wrappes the annoying long type.
typealias SearchsModuleComposer = ModuleComposer<
    SearchsView<SearchsPresenter>,
    SearchsPresenter,
    SearchsInteractor<SearchsPresenter>,
    SearchsWireframe<SearchsPresenter>
>

/**
 This method retuns the basic games list entire section.
 */
func getSearchsModule() -> SearchsModuleComposer {
    let module: SearchsModuleComposer = SearchsModuleComposer()
    return module
}
