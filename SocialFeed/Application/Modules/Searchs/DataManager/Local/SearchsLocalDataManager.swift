//
//  SearchsLocalDataManager.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/26/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

import RealmSwift

class SearchsLocalPostDataManager {
    /// This method stores into real a search.
    /// - parameters: withText: Buffer to save into db.
    func loadAllSearchs() -> [LocalSearchModel] {
        // Get REALM!!!!!!!
        let realm: Realm = try! Realm()
        // Load all the items.
        let values = realm.objects(LocalSearchModel.self)
        var outValues: [LocalSearchModel] = []
        // Generate a deatached copy of the real object.
        for item in values {
            outValues.append(LocalSearchModel(value: item))
        }
        return outValues
    }
}

