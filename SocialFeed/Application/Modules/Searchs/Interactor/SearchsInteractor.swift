//
//  SearchsInteractor.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/26/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

typealias SearchsInteractorPresenterTypes =
    ViewControllerEvents &
    SearchsPresenterRetreive &
    PresenterLinks

final class SearchsInteractor<TPresenter: SearchsInteractorPresenterTypes> {
    // A reference to the presenter.
    private weak var presenterRef: TPresenter? = nil
    // Data manager
    var dataManager: SearchsLocalPostDataManager = SearchsLocalPostDataManager()
}

// MARK: - SearchsPresenterEvents

extension SearchsInteractor: SearchsPresenterEvents {
    /// This method loads all the searchs from database.
    func loadAllSearchs() {
        let searchs: [LocalSearchModel] = dataManager.loadAllSearchs()
        presenterRef?.retrieveSearchs(searchs: searchs.map(
        { (lsearch:LocalSearchModel) -> SearchModel in
            return SearchModel(text: lsearch.search)
        }))
    }
}

// MARK: - Force initializer

extension SearchsInteractor: Initializer { }

// MARK: - Interactor Links

extension SearchsInteractor: InteractorLinks {
    
    var presenter: PresenterLinks? {
        get { return presenterRef }
        set { presenterRef = newValue as? TPresenter }
    }
}
