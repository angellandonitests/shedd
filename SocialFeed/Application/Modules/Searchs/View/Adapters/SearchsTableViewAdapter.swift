//
//  SearchsTableViewAdapter.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/26/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

import UIKit

final class SearchsTableViewAdapter: NSObject {
    var tableView: UITableView? = nil
    var items: [String]? = nil
    
    // MARK: - Public methods
    
    /// This method reloads the entire table.
    func reload() { tableView?.reloadData() }
}

// MARK: - Table View Data Source

extension SearchsTableViewAdapter: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int { return 1 }
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        guard let strongItems = items else { return 0 }
        return strongItems.count
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "UITableViewCell")!
        guard items != nil else { return cell }
        SearchsDrawer.drawSearchsTabelViewCell(cell: cell, andData: items![indexPath.row])
        return cell
    }
}
