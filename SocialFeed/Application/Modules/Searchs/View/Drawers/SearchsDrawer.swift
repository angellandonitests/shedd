//
//  SearchsDrawer.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/26/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

import UIKit

enum SearchsDrawer {
    /// This method setups the data into the cell.
    static func drawSearchsTabelViewCell(cell: UITableViewCell,
                                      andData data: String) {
        cell.textLabel?.text = data
    }
}
