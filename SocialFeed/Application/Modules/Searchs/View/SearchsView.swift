//
//  SearchsView.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/26/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

import UIKit

typealias SearchsViewPresenterTypes =
    ViewControllerEvents &
    SearchsPresenterRetreive &
    PresenterLinks

final class SearchsView<TPresenter: SearchsViewPresenterTypes>: SFViewController {
    
    // MARK: - Architecture vars
    
    private var presenterRef: TPresenter? = nil
    
    // MARK: - Components
    
    private var tableView: UITableView = UITableView(frame: .zero,
                                                     style: .grouped)
    
    // MARK: - Vars
    
    private var tableViewAdapter: SearchsTableViewAdapter = SearchsTableViewAdapter()
    
    // MARK: - View controller methods

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        presenterRef?.onViewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenterRef?.onViewWillAppear()
    }
    
    // MARK: - Private methods
    
    private func setup() {
        // Add components
        view.addSubview(tableView)
        // Setup table view.
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "UITableViewCell")
        tableView.translatesAutoresizingMaskIntoConstraints = false
        // Setup adapter.
        tableViewAdapter.tableView = tableView
        tableView.delegate = tableViewAdapter
        tableView.dataSource = tableViewAdapter
        // Setup constraints
        NSLayoutConstraint.activate([
            view.leftAnchor.constraint(equalTo: tableView.leftAnchor),
            view.rightAnchor.constraint(equalTo: tableView.rightAnchor),
            view.topAnchor.constraint(equalTo: tableView.topAnchor),
            view.bottomAnchor.constraint(equalTo: tableView.bottomAnchor),
        ])
        
        title = Localized.load(withKey: "SearchsListSectionTitle", andTable: "Basic")
    }
}

// MARK: - Searchs View Update Events

extension SearchsView: SearchsViewUpdateEvents {
    /// This method updates the table view with the correct content.
    func updateSearchList(posts: [String]) {
        tableViewAdapter.items = posts
        tableViewAdapter.reload()
    }
}

// MARK: - Force Initializer

extension SearchsView: Initializer {  }


// MARK: - View Links

extension SearchsView: ViewLinks {
    
    var presenter: PresenterLinks? {
        get { return presenterRef }
        set { presenterRef = newValue as? TPresenter }
    }
}
