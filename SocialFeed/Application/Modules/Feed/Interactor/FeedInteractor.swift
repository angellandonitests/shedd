//
//  FeedInteractor.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/25/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

/// Requerimients needed to setup the presenter into the interactor.
typealias FeedInteractorPresenterTypes =
    ViewControllerEvents &
    FeedPresenterRetreive &
    PresenterLinks

final class FeedInteractor<TPresenter: FeedInteractorPresenterTypes> {
    // A reference to the presenter.
    private weak var presenterRef: TPresenter? = nil
    // Social API pointer.
    var socialAPI: SocialAPI = SocialAPI()
    var localPostDataManager: LocalPostDataManager = LocalPostDataManager()
}

// MARK: - Feed Presenter Events

extension FeedInteractor: FeedPresenterEvents {
    /// This method loads the basic feeds needed to show in
    /// the main view.
    func loadBasicFeeds() {
        socialAPI.loadFeeds(withLimit: LaunchConfig.numberOfFeedsToLoad) {
            (posts: [SocialAPIPostModel]) in
            self.presenterRef?.retreivePosts(
                posts: posts.map({ (post: SocialAPIPostModel) -> PostModel in
                return PostModel(text: post.text, image: post.image)
            }))
        }
    }
    
    /// This methoid search all the posts that mach with the text.
    /// - parameters: withText: Text to search.
    func loadSpecificFeed(withText text: String) {
        // Store data using the Data manager.
        localPostDataManager.saveNewSearch(withText: text)
        // Search using the API.
        socialAPI.searchFeeds(byText: text) { (posts: [SocialAPIPostModel]) in
            self.presenterRef?.retreivePosts(
                posts: posts.map({ (post: SocialAPIPostModel) -> PostModel in
                return PostModel(text: post.text, image: post.image)
            }))
        }
    }
}

// MARK: - Force initializer

extension FeedInteractor: Initializer { }

// MARK: - Interactor Links

extension FeedInteractor: InteractorLinks {
    
    var presenter: PresenterLinks? {
        get { return presenterRef }
        set { presenterRef = newValue as? TPresenter }
    }
}
