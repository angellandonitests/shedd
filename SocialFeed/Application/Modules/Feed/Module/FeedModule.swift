//
//  FeedModule.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/25/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

// Wrappes the annoying long type.
typealias FeedModuleComposer = ModuleComposer<
    FeedView<FeedPresenter>,
    FeedPresenter,
    FeedInteractor<FeedPresenter>,
    FeedWireframe
>

/**
 This method retuns the basic games list entire section.
 */
func getFeedModule() -> FeedModuleComposer {
    let module: FeedModuleComposer = FeedModuleComposer()
    return module
}
