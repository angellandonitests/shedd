//
//  FeedView.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/25/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

import UIKit

/// Requerimients needed to setup the presenter into the view.
typealias FeedViewPresenterTypes =
    ViewControllerEvents &
    FeedViewActions &
    FeedPresenterRetreive &
    PresenterLinks

final class FeedView<TPresenter: FeedViewPresenterTypes>: SFXibViewController {
    
    // MARK: - Architecture vars
    
    private var presenterRef: TPresenter? = nil

    // MARK: - Components
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchField: UITextField!
    
    // MARK: - Vars
    
    private var tableViewAdapter: FeedTableViewAdapter = FeedTableViewAdapter()
    
    // MARK: - View controller methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        presenterRef?.onViewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenterRef?.onViewWillAppear()
    }
    
    // MARK: - Private methods
    
    /// This method setups the entire component.
    private func setup() {
        // Setup the table view adapter.
        tableViewAdapter.tableView = tableView
        tableView.delegate = tableViewAdapter
        tableView.dataSource = tableViewAdapter
        // Registre basic cell.
        tableView.register(
            UINib(nibName: "FeedTableViewCell", bundle: Bundle.main),
            forCellReuseIdentifier: "FeedTableViewCell")
        // Setup the title.
        title = Localized.load(withKey: "PostListSectionTitle",andTable: "Basic")
    }
    
    // MARK: - Actions
    
    @IBAction func onTapSearchButton(_ sender: UIButton) {
        presenterRef?.onTapSearchButton()
    }
}

// MARK: - Feed view update events

extension FeedView: FeedViewUpdateEvents {
    /// This method update the list of posts.
    /// - paramters: posts: Posts to show.
    func updatePostsList(posts: [[String : String]]) {
        tableViewAdapter.items = posts
        DispatchQueue.main.async {
            self.tableViewAdapter.reload()
        }
    }
}

// MARK: - Feed view data handler

extension FeedView: FeedViewDataHandler {
    /// This method returns the text that is in the search text field.
    func getSearchBarText() -> String { return searchField.text! }
}

// MARK: - Force Initializer

extension FeedView: Initializer {  }


// MARK: - View Links

extension FeedView: ViewLinks {
    
    var presenter: PresenterLinks? {
        get { return presenterRef }
        set { presenterRef = newValue as? TPresenter }
    }
}
