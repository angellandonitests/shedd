//
//  FeedViewUpdateEvents.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/25/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

protocol FeedViewUpdateEvents: class {
    func updatePostsList(posts: [[String:String]])
}
