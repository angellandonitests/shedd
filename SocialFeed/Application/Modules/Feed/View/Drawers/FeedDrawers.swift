//
//  FeedDrawers.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/26/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

import Alamofire

enum FeedDrawer {
    /// This method setups the data into the cell.
    static func drawFeedTabelViewCell(cell: FeedTableViewCell,
                                      andData data: [String:String]) {
        cell.postText.text = data["text"]
        guard let imageURL = data["image"] else { return }
        PostDataManager().loadPostImageFromNet(withPath: imageURL) { (image: UIImage) in
            cell.iconImage.image = image
        }
    }
}
