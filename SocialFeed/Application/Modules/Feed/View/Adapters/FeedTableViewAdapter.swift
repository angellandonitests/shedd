//
//  FeedTableViewAdapter.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/25/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

import UIKit

final class FeedTableViewAdapter: NSObject {
    var tableView: UITableView? = nil
    var items: [[String : String]]? = nil
    
    // MARK: - Public methods
    
    /// This method reloads the entire table.
    func reload() { tableView?.reloadData() }
}

// MARK: - Table View Data Source

extension FeedTableViewAdapter: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int { return 1 }
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        guard let strongItems = items else { return 0 }
        return strongItems.count
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard items != nil else {
            print("There was an error tring to setup / create cell")
            return UITableViewCell()
        }
        // Create the cell.
        let cell: FeedTableViewCell = tableView.dequeueReusableCell(
            withIdentifier: "FeedTableViewCell") as! FeedTableViewCell
        FeedDrawer.drawFeedTabelViewCell(cell: cell, andData: items![indexPath.row])
        return cell
    }
    
}
