//
//  PostDataManager.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/26/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

import UIKit
import Alamofire

class PostDataManager {
    func loadPostImageFromNet(withPath path: String,
                                     andCallback callback: @escaping (UIImage) -> Void) {
        Alamofire.request(path).responseData { (response) in
            guard response.error == nil else {
                print("There was an error loading the image.")
                return
            }
            
            if let data = response.data {
                if let image = UIImage(data: data, scale: 1) {
                    callback(image)
                }
            }
        }
    }
}
