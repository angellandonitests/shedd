//
//  LocalPostDataManager.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/26/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

import RealmSwift

class LocalPostDataManager {
    /// This method stores into real a search.
    /// - parameters: withText: Buffer to save into db.
    func saveNewSearch(withText text: String) {
        // Get REALM!!!!!!!
        let realm: Realm = try! Realm()
        // Check if the text is empty, if it is empty it must return
        // in order to avoid add a new empty element.
        guard !text.isEmpty else {
            print("Error: the buffer is empty.")
            return
        }
        try! realm.write() {
            // Create a new item.
            let newLocalSeach: LocalSearchModel = LocalSearchModel()
            newLocalSeach.search = text
            realm.add(newLocalSeach)
        }
    }
}
