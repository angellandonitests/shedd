//
//  FeedPresenter.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/25/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

final class FeedPresenter {
    private weak var viewRef: FeedView<FeedPresenter>? = nil
    private var interactorRef: FeedInteractor<FeedPresenter>? = nil
    private var wireframeRef: FeedWireframe? = nil
}

// MARK: - View Controller Events

extension FeedPresenter: ViewControllerEvents {
    
    func onViewDidLoad() {
        // Do something when the view is lodaded.
    }
    
    func onViewWillAppear() {
        interactorRef?.loadBasicFeeds()
    }
}

// MARK: - Feed View Actions

extension FeedPresenter: FeedViewActions {
    /// This method will be executed when the user taps on the
    /// search button.
    func onTapSearchButton() {
        guard let strongViewRef = viewRef else { return }
        interactorRef?.loadSpecificFeed(withText: strongViewRef.getSearchBarText())
    }
}

// MARK: - Feed Presenter Retreive

extension FeedPresenter: FeedPresenterRetreive {
    /// This method obtains all the list of posts and
    /// setup the view.
    func retreivePosts(posts: [PostModel]) {
        // Send the data to the view.
        viewRef?.updatePostsList(posts: posts.map({ (post: PostModel) -> [String:String] in
            return [
                "text": post.text,
                "image": post.image
            ]
        }))
    }
}

// MARK: - Force initializer

extension FeedPresenter: Initializer {  }

// MARk: - Presenter links

extension FeedPresenter: PresenterLinks {
    
    var view: ViewLinks? {
        get { return viewRef }
        set { viewRef = newValue as? FeedView<FeedPresenter> }
    }
    
    var interactor: InteractorLinks? {
        get { return interactorRef }
        set { interactorRef = newValue as? FeedInteractor<FeedPresenter> }
    }
    
    var wireframe: WireframeLinks? {
        get { return wireframeRef }
        set { wireframeRef = newValue as? FeedWireframe }
    }
}
