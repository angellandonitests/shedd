//
//  FeedWireframe.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/25/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

final class FeedWireframe {
    private weak var presenterRef: FeedPresenter? = nil
}

// MARK: - Force initializer

extension FeedWireframe: Initializer { }

// MARK: - Wireframe Links

extension FeedWireframe: WireframeLinks {
    
    var presenter: PresenterLinks? {
        get { return presenterRef }
        set { presenterRef = newValue as? FeedPresenter }
    }
}
