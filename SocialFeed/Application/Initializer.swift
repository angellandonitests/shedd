//
//  Initializer.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/24/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

import UIKit

/// This function inits the application
func initApplication() -> SFWindow {
    // Alloc a new window.
    let sfWindow: SFWindow = SFWindow()
    
    // Alloc a new module.
    let mainModule = LaunchConfig.mainModule.init()
    // Take the main class allocate it and setup it as the
    // root view controller.
    sfWindow.rootViewController = mainModule.view

    // Return the window with all the configurations.
    return sfWindow
}
