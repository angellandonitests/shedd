//
//  LocalSearchModel.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/26/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

import Foundation
import RealmSwift

class LocalSearchModel: Object {
    @objc var search: String = ""
}
