//
//  SocialAPIState.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/25/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

enum SocialAPIState {
    case Twitter
    case Facebook
    
    /// This method returns the correct API handler depending the
    /// state of the login.
    /// - returns: The API handler.
    func getClient() -> SocialAPIHandler {
        switch self {
        case .Twitter:
            return TwitterClient.shared
        case .Facebook:
            return FacebookClient.shared
        }
    }
}
