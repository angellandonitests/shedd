//
//  FacebookClient.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/26/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

import FacebookLogin
import FacebookCore

/// This class wraps the twitter client.
final class FacebookClient {
    /// Shared instance.
    static let shared: FacebookClient = FacebookClient()
    private var loginManager: LoginManager = LoginManager()
    
    private var accessToken: AccessToken? = nil
    
    /// Init the client.
    private init() {

    }
}

// MARK: - Social API Handler

extension FacebookClient: SocialAPIHandler {
    
    /// This method retuns the token.
    func getToken() -> String { return "Facebook" }
    
    /// This method logins the user into twitter.
    /// - parameters: callback: Lambda executed when the user login in to the app.
    func login(callback: @escaping (Bool) -> Void) {
        loginManager.logIn(readPermissions: [.publicProfile, .userPosts],
                           viewController: (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController
        ) { loginResult in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success( _,  _, let accessToken):
                self.accessToken = accessToken
                callback(true)
            }
        }
    }
    
    /// This method loads a specific number of feeds.
    /// - parameters: withLimit: Number of feeds.
    ///               andCallback: lambda used to get the items.
    func loadFeeds(withLimit limit: Int,
                   andCallback callback: @escaping ([SocialAPIPostModel]) -> Void) {
        let connection = GraphRequestConnection()
        connection.add(GraphRequest(graphPath: "/me/feed")) { httpResponse, result in
            switch result {
            case .success(let response):
                if let data = response.dictionaryValue!["data"] as? [[String:String]] {
                    callback(data.map({ (item: [String: String]) -> SocialAPIPostModel in
                        var post: SocialAPIPostModel = SocialAPIPostModel()
                        if let message = item["message"] {
                            post.text = message
                        }
                        return post
                    }))
                }
            case .failed(let error):
                print("Graph Request Failed: \(error)")
            }
        }
        connection.start()
    }
    
    /// This method searchs a specific tweet.
    /// - parameters: text: The buffer to search.
    ///               callback: Lambda executed when twitte sends data.
    func searchFeeds(withText text: String,
                     andCallback callback: @escaping ([SocialAPIPostModel]) -> Void) {
        // NOTE: Facebook does not support search posts using a specific text.
        // This method will not be implemented and it gonna call the callback in
        // order to execute the correct flow, but it gonna send an empty array.
        callback([SocialAPIPostModel]())
    }
}
