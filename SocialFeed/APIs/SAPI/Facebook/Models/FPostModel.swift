//
//  FacebookModel.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/26/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

struct FPostModel {
    var time: String = ""
    var id: String = ""
    var message: String = ""
}
