//
//  SocialAPI.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/24/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

class SocialAPI {
    
    // MARK: - Properties
    
    /// Social media layer.
    private static var socialAPIHandler: SocialAPIHandler? = nil
    
    // MARK: - Public methods
    
    /// This method takes a parameter and using this parameter
    /// it creates the correct client depending the required social media.
    /// - parameters: socialMedia: The social media needed.
    func setup(socialMedia state: SocialAPIState) {
        SocialAPI.socialAPIHandler = state.getClient()
    }
    
    func login(callback: @escaping (Bool) -> Void ) {
        // Login into the social media.
        SocialAPI.socialAPIHandler?.login { (state: Bool) in
            callback(state)
        }
    }
    
    func loadFeeds(withLimit limit: Int,
                   andCallback callback: @escaping ([SocialAPIPostModel]) -> Void) {
        SocialAPI.socialAPIHandler?.loadFeeds(withLimit: limit, andCallback: callback)
    }
    
    func searchFeeds(byText text: String,
                     andCallback callback: @escaping ([SocialAPIPostModel]) -> Void) {
        SocialAPI.socialAPIHandler?.searchFeeds(withText: text,
                                     andCallback: { (post: [SocialAPIPostModel]) in
            callback(post)
        })
    }
    
    /// This var returns the token of the social api.
    var token: String? { return SocialAPI.socialAPIHandler?.getToken() }
}
