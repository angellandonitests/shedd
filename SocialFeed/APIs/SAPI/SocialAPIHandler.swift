//
//  SocialAPIHandler.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/24/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

protocol SocialAPIHandler: class {
    /// This method returns the token associated with the
    /// social instance.
    func getToken() -> String
    /// This method executs the callback when the user logins into
    /// a social media
    func login(callback: @escaping (Bool) -> Void)
    /// This method loads a list of feeds.
    func loadFeeds(withLimit limit: Int, andCallback callback: @escaping ([SocialAPIPostModel]) -> Void)
    /// Search a specific tweet.
    func searchFeeds(withText text: String, andCallback callback: @escaping ([SocialAPIPostModel]) -> Void)
}
