//
//  TwitterParcer.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/25/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

import Swifter

enum TwitterParcer {
    
    /// This method parses and returns a list of tweets.
    /// - parameters: withJSon: raw json data.
    /// - returns: An array of tweets.
    static func twitterParceTimeLineTweets(withJSon json: JSON) -> [TweetModel] {
        guard let tweets = json.array else { return [TweetModel]() }
        
        let tweetArray: [TweetModel] = tweets.map { (jsonItem: JSON) -> TweetModel in
            var newTweet: TweetModel = TweetModel()
            if let text = jsonItem["text"].string {
                newTweet.text = text
            }
            if let user = jsonItem["user"].object {
                if let image = user["profile_image_url"] {
                    if let imageSource = image.string {
                        newTweet.image = imageSource
                    }
                }
            }
            return newTweet
        }
        
        return tweetArray
    }
    
    /// This method returns convert from tweets to social api post
    /// - parameters: tweets: An array of tweets.
    /// - retuns: An array of API Post model.
    static func fromTweetToSocialPost(tweets: [TweetModel]) -> [SocialAPIPostModel] {
        return tweets.map({ (tweet: TweetModel) -> SocialAPIPostModel in
            var post: SocialAPIPostModel = SocialAPIPostModel()
            post.text = tweet.text
            post.image = tweet.image
            return post
        })
    }
}
