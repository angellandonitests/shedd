//
//  TwitterClient.swift
//  SocialFeed
//
//  Created by Angel Landoni on 11/24/17.
//  Copyright © 2017 Angel Landoni. All rights reserved.
//

import TwitterKit
import Swifter

/// This class wraps the twitter client.
final class TwitterClient {
    /// Shared instance.
    static let shared: TwitterClient = TwitterClient()
    private var client: Swifter? = nil
    
    /// Init the client.
    private init() {
        // Setup Twitter stuffs.
        client = Swifter(consumerKey: TwitterConfig.consumerKey,
                         consumerSecret: TwitterConfig.consumerSecretkey)
    }
}

// MARK: - Social API Handler

extension TwitterClient: SocialAPIHandler {

    /// This method retuns the token.
    func getToken() -> String { return "Twitter" }
   
    /// This method logins the user into twitter.
    /// - parameters: callback: Lambda executed when the user login in to the app.
    func login(callback: @escaping (Bool) -> Void) {
        client?.authorize(with: URL(string: TwitterConfig.deepLink + "://success")!,
                          presentFrom: (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController,
                          success: { _, _ in
            callback(true)
        }, failure: { error in
            callback(false)
        })
    }
    
    /// This method loads a specific number of feeds.
    /// - parameters: withLimit: Number of feeds.
    ///               andCallback: lambda used to get the items.
    func loadFeeds(withLimit limit: Int,
                   andCallback callback: @escaping ([SocialAPIPostModel]) -> Void) {
        client?.getHomeTimeline(count: limit, success: { json in
            callback(TwitterParcer.fromTweetToSocialPost(
                tweets: TwitterParcer.twitterParceTimeLineTweets(withJSon: json)))
        }, failure: { error in
            print("Error")
        })
    }
    
    /// This method searchs a specific tweet.
    /// - parameters: text: The buffer to search.
    ///               callback: Lambda executed when twitte sends data.
    func searchFeeds(withText text: String,
                    andCallback callback: @escaping ([SocialAPIPostModel]) -> Void) {
        client?.searchTweet(using: text,
                            geocode: nil,
                            lang: nil,
                            locale: nil,
                            resultType: nil,
                            count: nil,
                            until: nil,
                            sinceID: nil,
                            maxID: nil,
                            includeEntities: nil,
                            callback: nil,
                            tweetMode: TweetMode.default,
                            success: { (json: JSON, settings: JSON) in
            callback(TwitterParcer.fromTweetToSocialPost(
                    tweets: TwitterParcer.twitterParceTimeLineTweets(withJSon: json)))
        }, failure: { (error: Error) in
            print("Error searching tweet", error)
        })
    }
}
